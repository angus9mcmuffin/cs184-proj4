#include "include.h"

//GLOBALS
using namespace Eigen;
using namespace std;
Vector3f goal;

#define PI 3.14159265


void initializeRendering()
{
	glfwInit();

}


//****************************************************
// A routine to set a pixel by drawing a GL point.  This is not a
// general purpose routine as it assumes a lot of stuff specific to
// this example.
//****************************************************
void setPixel(float x, float y, GLfloat r, GLfloat g, GLfloat b) {
	glColor3f(r, g, b);
	glVertex2f(x + 0.5, y + 0.5);  // The 0.5 is to target pixel centers
								   // Note: Need to check for gap bug on inst machines.
}
void display(int n)
{

}

int main(int argc, char** argv)
{
	display(4);
	return 0;
}