#pragma once
#ifndef __IKNODE_H_INCLDUED__
#define __IKNODE_H_INCLUDED__
#include "include.h"
class IKnode
{

public:
	IKnode *parent;
	IKnode *child;
	Vector3f location;
	IKnode();
	IKnode(Vector3f, IKnode *, IKnode *);
	void setParent(IKnode *);
	void setChild(IKnode *);
	void setLocation(Vector3f);
	void draw();
};
#endif
