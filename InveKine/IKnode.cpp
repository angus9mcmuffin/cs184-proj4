#include "IKnode.h"

IKnode::IKnode()
{
	location = Vector3f(0.0f, 0.0f, 0.0f);
	parent = 0;
	child = 0;
}

IKnode::IKnode(Vector3f a, IKnode * b, IKnode * c)
{
	location = a;
	parent = b;
	child = c;
}

void IKnode::setParent(IKnode *prev)
{
	parent = prev;
}

void IKnode::setChild(IKnode *next)
{
	child = next;
}

void IKnode::setLocation(Vector3f a)
{
	location = a;
}

void IKnode::draw()
{

}
