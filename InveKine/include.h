#pragma once
#ifndef INCLUDE_H
#define INCLUDE_H

#include <vector>
#ifdef _WIN32
#include <windows.h>
#else
#include <sys/time.h>
#endif
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <vector>
#include <list>
#include <cmath>
// Credits to Implementation of matrix, matrix math, exponential maps and almost all miscellaneous math functions to http://bitbucket.org/eigen/eigen/get/3.3.1.tar.bz2

#include <Eigen/Dense>
#include <GLFW/glfw3.h>
#include <GL/glew.h>
using namespace std;
using namespace Eigen;
#endif